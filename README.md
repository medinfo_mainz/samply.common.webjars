# Samply Common Static

## General information
This project offers static javascript, css and image files. It bundles well known libraries, e.g. bootstrap, into jar files that can be used in Servlet containers (>= 3.0).
We decided to build our own webjars to be more flexible. Some OSSE projects use primefaces which uses a specific version of jQuery.

As of May 31st, 2017, the repository has moved to https://bitbucket.org/medicalinformatics/samply.common.webjar. Please see the following help article on how to change the repository location in your working copies:

* https://help.github.com/articles/changing-a-remote-s-url/

If you have forked Mainzelliste-OSSE in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).